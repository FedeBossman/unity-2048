﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackgroundMusic : MonoBehaviour {

    public AudioClip mainClip;
    public AudioClip gameOverClip;
    public GameObject musicButton;
    public Sprite volumeSprite;
    public Sprite muteSprite;

    private AudioSource source;
    private bool mute = false;

	// Use this for initialization
	void Start () {
        source = this.GetComponent<AudioSource>();
        // Get last saved value
        mute = PlayerPrefs.GetInt("BackgroundMusicMute", 0) > 0;
        // Set icon accordingly
        if (mute)
        {
            musicButton.GetComponent<Image>().sprite = muteSprite;
        }
        else
        {
            musicButton.GetComponent<Image>().sprite = volumeSprite;
        }
    }
	
	// Update is called once per frame
	void Update () {
        var gameState = GameController.instance.getCurrentState();
        if (gameState == GameController.GameState.End && source.clip != gameOverClip)
        {
            source.clip = gameOverClip;
            source.loop = false;
            source.volume = 0.8f;
            if (!mute) source.Play();
        } else if (gameState != GameController.GameState.End && source.clip != mainClip)
        {
            source.clip = mainClip;
            source.loop = true;
            source.volume = 1.2f;
            if (!mute) source.Play();
        }
	}

    /// <summary>
    /// Mutes or unmutes background music. Saves state and changes icon for feedback
    /// </summary>
    public void ToggleMute()
    {
        mute = !mute;
        if (mute)
        {
            source.Stop();
            musicButton.GetComponent<Image>().sprite = muteSprite;
        } else
        {
            source.Play();
            musicButton.GetComponent<Image>().sprite = volumeSprite;
        }
        PlayerPrefs.SetInt("BackgroundMusicMute", mute ? 1 : 0);
    }
}
