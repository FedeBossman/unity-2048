﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class CellController : MonoBehaviour {

    public Number number = null;
    private bool animatingMerge = false;
    private Number mergingNumber;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool SpawnNumber(GameObject numberPrefab)
    {
        if (number != null)
        {
            // Object already exists
            return false;
        }
        // Create a number object and place it in cell
        GameObject numberObject = GameObject.Instantiate(numberPrefab);
        numberObject.transform.SetParent(transform.parent, false);
        numberObject.transform.localPosition = transform.localPosition;
        number = new Number(numberObject);
        return true;
    }

    internal bool HasNumber()
    {
        return number != null;
    }

    public bool CanMoveTo(CellController cell)
    {
        return !cell.HasNumber() || cell.CanMerge(number.value);
    }

    public bool MoveTo(CellController cell)
    {
        if (this.CanMoveTo(cell))
        {
            cell.Merge(number);
            number = null;
            // TODO: animate number
            return true;
        } else
        {
            return false;
        }
    }

    private void Merge(Number number)
    {
        if (HasNumber())
        {
            var mo = number.numberObject.GetComponent<MovingObject>();
            mo.Move(transform.localPosition.x, transform.localPosition.y);
            // Mark as merged before animation for the rest of cells to know during calculation
            this.number.merged = true;

            // Only need to add value
            this.MergeAnim(number);           

            // Update score
            GameController.instance.AddScore(this.number.value);
        } else
        {
            // Get reference
            this.number = number;
            // Replace
            var mo = this.number.numberObject.GetComponent<MovingObject>();
            mo.Move(transform.localPosition.x, transform.localPosition.y);
        }
    }

    /// <summary>
    /// Checks if animations are still in running
    /// </summary>
    /// <returns></returns>
    internal bool isMoving()
    {
        if (HasNumber())
        {
            var mo = this.number.getMovingObject();
            if (mo.isMoving()) return true;
        }
        return animatingMerge && mergingNumber != null;
    }

    private bool CanMerge(int value)
    {
        return HasNumber() && !number.merged && (value == number.value);
    }

    internal void NewTurn()
    {
        if (HasNumber())
        {
            number.merged = false;
        }
    }

    internal void NewGame()
    {
        if (HasNumber())
        {
            Destroy(number.numberObject);
            number = null;
        }
    }


    protected bool MergeAnim(Number orgNumber)
    {
        //Color start = number.numberObject;
        animatingMerge = true;
        mergingNumber = orgNumber;
        StartCoroutine(SmoothColors(Number.Colors[this.number.value*2]));
        StartCoroutine(DestroyAfterMerge());
        return true;
    }

    protected IEnumerator SmoothColors(Color end)
    {
        float t = 0f;
        while (t < 1)
        { // while t below the end limit...
          // increment it at the desired rate every update:
            number.numberObject.GetComponent<Image>().color = Color.Lerp(number.numberObject.GetComponent<Image>().color, end, t);
            mergingNumber.numberObject.GetComponent<Image>().color = Color.Lerp(number.numberObject.GetComponent<Image>().color, end, t);
            t += Time.deltaTime / 0.10f;
            yield return null;
        }

        animatingMerge = false;
    }

    protected IEnumerator DestroyAfterMerge()
    {
        while (animatingMerge)
        {
            yield return null;
        }
        this.number.value += mergingNumber.value;

        Destroy(mergingNumber.numberObject);
        mergingNumber = null;

        // TODO: destroy old number and respawn to automatically pop and override drawing order
        this.number.getMovingObject().MergePop();
    }

}
