﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController instance = null;
    public GameObject scoreText;
    public GameObject highScoreText;
    public GameObject logs;
    public GameObject restartButton;
    public GameObject numberPrefab;
    public CellController[] cellSetup = new CellController[16];

    public GameState currentState = GameState.Start;
    
    private CellController[][] cells;
    private int score;
    private int highscore;
    private bool newHighScore = false;
    private bool newGame = false;
    private Vector2 touchOrigin = -Vector2.one;

    // Use this for initialization
    void Start () {
        // Initialize singleton
        if (instance == null)
        {
            instance = this;
        } else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        // Load highscore
        highscore = PlayerPrefs.GetInt("HighScore", 0);
        UpdateHighScore();

        // Initialize cells
        InitializeCells();

        // Start game
        RestartGame();
    }

    // Update is called once per frame
    void Update () {
        if (currentState == GameState.Waiting)
        {
            ProcessWaitingState();
        } else if (currentState == GameState.Moving)
        {
            if (newGame)
            {
                // Remove text
                newGame = false;
                AddLog("");
            }

            var moving = false;
            foreach (var cellRow in cells)
            {
                foreach (var cell in cellRow)
                {
                    if (cell.isMoving())
                    {
                        moving = true;
                    }
                }
            }
            if (!moving)
            {
                // Spawn the next numer and wait for movement or end game
                SpawnNextNumber();
            }
        }
    }

    /// <summary>
    /// Waits for valid user input to process next movement
    /// </summary>
    private void ProcessWaitingState()
    {
        int horizontal = 0;
        int vertical = 0;
//#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR || true
        horizontal = Input.GetKeyUp(KeyCode.RightArrow) ? 1 : (Input.GetKeyUp(KeyCode.LeftArrow) ? -1 : 0);
        vertical = Input.GetKeyUp(KeyCode.UpArrow) ? 1 : (Input.GetKeyUp(KeyCode.DownArrow) ? -1 : 0);
        if (horizontal != 0) {
            vertical = 0;
        }
//#else
        if (Input.touchCount > 0)
        {
            Touch myTouch = Input.touches[0];
            if (myTouch.phase == TouchPhase.Began)
            {
                // Check viewport so that touches dont begin at top and bottom edges
                var viewportTouch = Camera.main.ScreenToViewportPoint(myTouch.position);
                if (viewportTouch.y > 0.1f && viewportTouch.y < 0.9f)
                {
                    touchOrigin = myTouch.position;
                }
            } else if (myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0)
            {
                Vector2 touchEnd = myTouch.position;
                float x = touchEnd.x - touchOrigin.x;
                float y = touchEnd.y - touchOrigin.y;
                touchOrigin.x = -1;
                if (Math.Abs(x) > 100 || Math.Abs(y) > 100)
                {
                    if (Math.Abs(x) > Math.Abs(y))
                    {
                        horizontal = x > 0 ? 1 : -1;
                    }
                    else
                    {
                        vertical = y > 0 ? 1 : -1;
                    }
                }

            }
        }
//#endif
        if (vertical > 0)
        {
            if (CanMoveUp())
            {
                // If movement is possible, apply and spawn next object
                MoveUp();
                currentState = GameState.Moving;
            }
        }
        else if (horizontal > 0)
        {
            if (CanMoveRight())
            {
                // If movement is possible, apply and spawn next object
                MoveRight();
                currentState = GameState.Moving;
            }
        }
        else if (vertical < 0)
        {
            if (CanMoveDown())
            {
                // If movement is possible, apply and spawn next object
                MoveDown();
                currentState = GameState.Moving;
            }
        }
        else if (horizontal < 0)
        {
            if (CanMoveLeft())
            {
                // If movement is possible, apply and spawn next object
                MoveLeft();
                currentState = GameState.Moving;
            }
        }
    }

    /// <summary>
    ///  Set cells in a matrix
    /// </summary>
    private void InitializeCells()
    {
        cells = new CellController[Globals.Rows][];
        for (int i = 0; i < Globals.Rows; i++)
        {
            // Rows
            cells[i] = new CellController[Globals.Columns];
            for (int j = 0; j < Globals.Columns; j++)
            {
                // Columns
                cells[i][j] = cellSetup[i * Globals.Rows + j];
            }
        }
    }

    /// <summary>
    /// Tries to spawn a number in a random cell
    /// </summary>
    public void SpawnNextNumber()
    {
        // Choose a random cell
        int row = UnityEngine.Random.Range(0, Globals.Rows);
        int column = UnityEngine.Random.Range(0, Globals.Columns);
        // try to spawn number in cell, change values if it existed there
        while (!cells[row][column].SpawnNumber(numberPrefab))
        {
            row = UnityEngine.Random.Range(0, Globals.Rows);
            column = UnityEngine.Random.Range(0, Globals.Columns);
        }
        // TODO: check if movements left, else end
        if (CanMove())
        {
            // Wait for next movement
            WaitForMovement();
        } else
        {
            EndGame();
        }
    }

    /// <summary>
    /// Checks if any movement is possible
    /// </summary>
    /// <returns></returns>
    private bool CanMove()
    {
        return CanMoveVertically() || CanMoveHorizontally();
    }

    /// <summary>
    /// Checks if a horizontal (left or right) movement is possible
    /// </summary>
    /// <returns></returns>
    private bool CanMoveHorizontally()
    {
        return CanMoveLeft() || CanMoveRight();
    }

    /// <summary>
    /// Checks if a vertical movement is possible
    /// </summary>
    /// <returns></returns>
    private bool CanMoveVertically()
    {
        return CanMoveUp() || CanMoveDown();
    }

    /// <summary>
    /// Cehecks if an up movement is possible
    /// </summary>
    /// <returns></returns>
    private bool CanMoveUp()
    {
        var canMove = false;
        for (int i = 0; i < Globals.Columns; i++)
        {
            if (CanMoveUpColumn(i))
            {
                canMove = true;
                break;
            }
        }
        return canMove;
    }

    /// <summary>
    /// Cehecks if a vertical movement on a specific column is possible
    /// </summary>
    /// <param name="col"></param>
    /// <returns></returns>
    private bool CanMoveUpColumn(int col)
    {
        int topRow = 0;
        bool canMove = false;
        // Iterate rows for column
        for (int row = 0; row < Globals.Rows; row++)
        {
            CellController cell = cells[row][col];
            if (cell.HasNumber())
            {
                // If its not empty, try to move it upwards
                CellController topCell;
                for (int i = topRow; i < row; i++)
                {
                    topCell = cells[i][col];
                    if (cell.CanMoveTo(topCell))
                    {
                        canMove = true;
                        break;
                    }
                    topRow++;
                    
                }
            }
        }
        return canMove;
    }

    /// <summary>
    /// Cehecks if a down movement is possible
    /// </summary>
    /// <returns></returns>
    private bool CanMoveDown()
    {
        var canMove = false;
        for (int i = 0; i < Globals.Columns; i++)
        {
            if (CanMoveDownColumn(i))
            {
                canMove = true;
                break;
            }
        }
        return canMove;
    }

    /// <summary>
    /// Cehecks if a down movement on a specific column is possible
    /// </summary>
    /// <param name="col"></param>
    /// <returns></returns>
    private bool CanMoveDownColumn(int col)
    {
        int bottomRow = Globals.Rows - 1;
        bool canMove = false;
        // Iterate rows for column
        for (int row = Globals.Rows - 1; row >= 0; row--)
        {
            CellController cell = cells[row][col];
            if (cell.HasNumber())
            {
                // If its not empty, try to move it upwards
                CellController destCell;
                for (int i = bottomRow; i > row; i--)
                {
                    destCell = cells[i][col];
                    if (cell.CanMoveTo(destCell))
                    {
                        canMove = true;
                        break;
                    }
                    bottomRow--;
                }
            }
        }
        return canMove;
    }

    /// <summary>
    /// Cehecks if a left movement is possible
    /// </summary>
    /// <returns></returns>
    private bool CanMoveLeft()
    {
        var canMove = false;
        for (int i = 0; i < Globals.Rows; i++)
        {
            if (CanMoveLeftRow(i))
            {
                canMove = true;
                break;
            }
        }
        return canMove;
    }

    /// <summary>
    /// Checks if a left movement on a specific row is possible
    /// </summary>
    /// <param name="col"></param>
    /// <returns></returns>
    private bool CanMoveLeftRow(int row)
    {
        int leftCol = 0;
        bool canMove = false;
        // Iterate columns from left to right
        for (int col = 0; col < Globals.Columns; col++)
        {
            CellController cell = cells[row][col];
            if (cell.HasNumber())
            {
                // If its not empty, try to move it upwards
                CellController destCell;
                for (int i = leftCol; i < col; i++)
                {
                    destCell = cells[row][i];
                    if (cell.CanMoveTo(destCell))
                    {
                        canMove = true;
                        break;
                    }
                    leftCol++;

                }
            }
        }
        return canMove;
    }

    /// <summary>
    /// Cehecks if a left movement is possible
    /// </summary>
    /// <returns></returns>
    private bool CanMoveRight()
    {
        var canMove = false;
        for (int i = 0; i < Globals.Rows; i++)
        {
            if (CanMoveRightRow(i))
            {
                canMove = true;
                break;
            }
        }
        return canMove;
    }

    /// <summary>
    /// Cehecks if a right movement on a specific row is possible
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private bool CanMoveRightRow(int row)
    {
        int leftCol = Globals.Columns - 1;
        bool canMove = false;
        // Iterate columns
        for (int col = Globals.Columns - 1; col >= 0; col--)
        {
            CellController cell = cells[row][col];
            if (cell.HasNumber())
            {
                // If its not empty, try to move it 
                CellController destCell;
                for (int i = leftCol; i > col; i--)
                {
                    destCell = cells[row][i];
                    if (cell.CanMoveTo(destCell))
                    {
                        canMove = true;
                        break;
                    }
                    leftCol--;
                }
            }
        }
        return canMove;
    }

    private void MoveUp()
    {
        for (int i = 0; i < Globals.Columns; i++)
        {
            MoveUpColumn(i);
        }
    }

    private void MoveUpColumn(int col)
    {
        int topRow = 0;
        // Iterate rows for column
        for (int row = 1; row < Globals.Rows; row++)
        {
            CellController cell = cells[row][col];
            if (cell.HasNumber())
            {
                // If its not empty, try to move it upwards
                CellController topCell = cells[topRow][col];
                while (topRow < row && !cell.MoveTo(topCell))
                {
                    topRow++;
                    topCell = cells[topRow][col];
                }
            }
        }
    }

    private void MoveDown()
    {
        for (int i = 0; i < Globals.Columns; i++)
        {
            MoveDownColumn(i);
        }
    }

    private void MoveDownColumn(int col)
    {
        int bottomRow = Globals.Rows - 1;
        // Iterate rows for column
        for (int row = Globals.Rows - 1; row >= 0; row--)
        {
            CellController cell = cells[row][col];
            if (cell.HasNumber())
            {
                // If its not empty, try to move it
                CellController destCell = cells[bottomRow][col];
                while (bottomRow > row && !cell.MoveTo(destCell))
                {
                    bottomRow--;
                    destCell = cells[bottomRow][col];
                }
            }
        }
    }


    private void MoveLeft()
    {
        for (int i = 0; i < Globals.Rows; i++)
        {
            MoveLeftRow(i);
        }
    }

    private void MoveLeftRow(int row)
    {
        int leftCol = 0;
        // Iterate rows for column
        for (int col = 1; col < Globals.Columns; col++)
        {
            CellController cell = cells[row][col];
            if (cell.HasNumber())
            {
                // If its not empty, try to move it upwards
                CellController destCell = cells[row][leftCol];
                while (leftCol < col && !cell.MoveTo(destCell))
                {
                    leftCol++;
                    destCell = cells[row][leftCol];
                }
            }
        }
    }


    private void MoveRight()
    {
        for (int i = 0; i < Globals.Columns; i++)
        {
            MoveRightRow(i);
        }
    }

    private void MoveRightRow(int row)
    {
        int rightCol = Globals.Columns - 1;
        // Iterate rows for column
        for (int col = Globals.Columns - 1; col >= 0; col--)
        {
            CellController cell = cells[row][col];
            if (cell.HasNumber())
            {
                // If its not empty, try to move it
                CellController destCell = cells[row][rightCol];
                while (rightCol > col && !cell.MoveTo(destCell))
                {
                    rightCol--;
                    destCell = cells[row][rightCol];
                }
            }
        }
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        Text text = scoreText.GetComponent<Text>();
        text.text = score.ToString();

        if (highscore < score)
        {
            if (!newHighScore)
            {
                // TODO: play sound
                newHighScore = true;
            }
            // Save highscore
            highscore = score;
            PlayerPrefs.SetInt("HighScore", score);
            Debug.Log("High Score is " + score);
            // Update UI
            UpdateHighScore();
        }
    }

    void UpdateHighScore()
    {
        Text text = highScoreText.GetComponent<Text>();
        text.text = highscore.ToString();
    }

    /// <summary>
    /// Waits for user movement. Cells merged status is restarted to false
    /// </summary>
    private void WaitForMovement()
    {
        foreach (var cellRow in cells)
        {
            foreach (var cell in cellRow)
            {
                cell.NewTurn();
            }
        }
        currentState = GameState.Waiting;
    }

    /// <summary>
    /// Game ends when there are no movements left. Change game state and show retry button
    /// </summary>
    private void EndGame()
    {
        currentState = GameState.End;
        AddLog("Game over");
        
        //restartButton.SetActive(true);
    }

    public void RestartGame()
    {
        //restartButton.SetActive(false);
        foreach (var cellRow in cells)
        {
            foreach (var cell in cellRow)
            {
                cell.NewGame();
            }
        }
        score = 0;
        newHighScore = false;
        UpdateScore();
        newGame = true;
        currentState = GameState.Start;
        SpawnNextNumber();
        AddLog("New game started");
    }

    public void ToggleMusic()
    {
        //restartButton.SetActive(false);
        AddLog("Music toggled");
        
    }

    public void AddLog(string text)
    {
        Debug.Log(text);
        logs.GetComponent<Text>().text = text;
    }

    public GameState getCurrentState()
    {
        return currentState;
    }

    public enum GameState
    {
        Start,
        Moving,
        Waiting,
        End
    }
}
