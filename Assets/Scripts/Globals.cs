﻿using UnityEngine;
using System.Collections;

public static class Globals
{
    public readonly static int Rows = 4;
    public readonly static int Columns = 4;
    public static readonly float AnimationDuration = 0.05f;

}