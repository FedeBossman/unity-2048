﻿using UnityEngine;
using System.Collections;
using System;

public class MovingObject : MonoBehaviour
{

    private float moveTime = (140.0f * 4) / 0.2f;
    private bool moving = false;

    // Use this for initialization
    protected virtual void Start()
    {
    }

    public bool Move(float xDir, float yDir)
    {
        moving = true;
        Vector2 start = transform.localPosition;
        Vector2 end = new Vector2(xDir, yDir);

        StartCoroutine(SmoothMovement(end));

        return true;
    }

    public bool MergePop()
    {
        StartCoroutine(MergePopAnimation(0.10f));

        return true;
    }

    protected IEnumerator SmoothMovement(Vector3 end)
    {
        float sqrRemainingDistance = (transform.localPosition - end).sqrMagnitude;
        while (sqrRemainingDistance > float.Epsilon)
        {
            Vector3 newPosition = Vector3.MoveTowards(transform.localPosition, end, moveTime * Time.deltaTime);
            this.transform.localPosition = newPosition;
            //transform.Translate(newPosition, transform.parent);
            sqrRemainingDistance = (transform.localPosition - end).sqrMagnitude;
            yield return null;
        }

        moving = false;
    }

    internal bool isMoving()
    {
        return moving;
    }

    IEnumerator MergePopAnimation(float time)
    {
        Vector3 originalScale = new Vector3(0.9f, 0.9f, 0.9f);
        Vector3 midScale = new Vector3(1.3f, 1.3f, 1.3f);
        Vector3 destinationScale = new Vector3(1.0f, 1.0f, 1.0f);

        var midTime = (time / 2);

        float currentTime = 0.0f;

        do
        {
            if (currentTime <= midTime)
            {
                transform.localScale = Vector3.Lerp(originalScale, midScale, currentTime / midTime);
            }
            else
            {
                transform.localScale = Vector3.Lerp(midScale, destinationScale, (currentTime - midTime) / (time - midTime));
            }
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);
        transform.localScale = destinationScale;
    }
}