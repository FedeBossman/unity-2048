﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Number {

    private static float colorStep = 1.0f / 25.0f;
    private static Color watColor = new Color(240/255.0f, 240/255.0f, 240/255.0f);
    private static Color startColor = new Color(250 / 255.0f, 250 / 255.0f, 250 / 255.0f);
    //private static Color endColor = new Color(249 / 255.0f, 45 / 255.0f, 0 / 255.0f);
    //private static Color endColor = new Color(0 / 255.0f, 45 / 255.0f, 249 / 255.0f);
    private static Color endColor = new Color(241 / 255.0f, 127 / 255.0f, 41 / 255.0f);

    public static readonly Dictionary<int, Color> Colors
        = new Dictionary<int, Color>
    {
        { 2, Color.Lerp(startColor, endColor, 0.0f * colorStep).linear},
        { 4, Color.Lerp(startColor, endColor, 3.0f * colorStep).linear},
        { 8, Color.Lerp(startColor, endColor, 6.0f * colorStep).linear},
        { 16, Color.Lerp(startColor, endColor, 9.0f * colorStep).linear},
        { 32, Color.Lerp(startColor, endColor, 12 * colorStep).linear},
        { 64, Color.Lerp(startColor, endColor, 15 * colorStep).linear},
        { 128, Color.Lerp(startColor, endColor, 17 * colorStep).linear},
        { 256, Color.Lerp(startColor, endColor, 19 * colorStep).linear},
        { 512, Color.Lerp(startColor, endColor, 21 * colorStep).linear},
        { 1024, Color.Lerp(startColor, endColor, 23 * colorStep).linear},
        { 2048, Color.Lerp(startColor, endColor, 25 * colorStep).linear}
    };

    public bool merged = false;

    public int _value;
    public int value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = value;
            Text text = this.numberObject.GetComponentInChildren<Text>();
            text.text = this.value.ToString();
            if (value > 8) text.color = new Color(240 / 255.0f, 240 / 255.0f, 240 / 255.0f);
            this.numberObject.GetComponent<Image>().color = Colors[value];
        }
    }
    public GameObject numberObject;


    public Number(GameObject numberObject)
    {
        this.numberObject = numberObject;
        this.value = (UnityEngine.Random.value > 0.78) ? 4 : 2;
    }

    public MovingObject getMovingObject()
    {
        return numberObject.GetComponent<MovingObject>();
    }
}
