﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PopIn : MonoBehaviour {
    void Start()
    {
       StartCoroutine(PopInAnimation(0.3f));
    }

    IEnumerator PopInAnimation(float time)
    {
        //transform.localPosition += new Vector3(50, -50, 0);
        //var c =  this.GetComponent<Image>().color;
        //this.GetComponent<Image>().color = Color.black;

        Vector3 originalScale = new Vector3(0.0f, 0.0f, 0.0f);
        Vector3 midScale = new Vector3(1.7f, 1.7f, 1.7f);
        Vector3 destinationScale = new Vector3(1.0f, 1.0f, 1.0f);

        var midTime = (time / 2);

        float currentTime = 0.0f;

        do
        {
            if (currentTime <= midTime)
            {
                transform.localScale = Vector3.Lerp(originalScale, midScale, currentTime / midTime);
            } else {
                transform.localScale = Vector3.Lerp(midScale, destinationScale, (currentTime - midTime) / (time - midTime));
            }
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);
        transform.localScale = destinationScale;
        //transform.position += new Vector3(0, 0, -1);
        //this.GetComponent<Image>().color = c;
    }
}